/*
@licstart  The following is the entire license notice for the
JavaScript code in this page.

Copyright (C) 2020 Moulinux

The JavaScript code in this page is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License (GNU GPL) as published by the Free Software
Foundation, either version 3 of the License, or (at your option)
any later version.  The code is distributed WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

As additional permission under GNU GPL version 3 section 7, you
may distribute non-source (e.g., minimized or compacted) forms of
that code without the copy of the GNU GPL normally required by
section 4, provided you include this license notice and a URL
through which recipients can access the Corresponding Source.


@licend  The above is the entire license notice
for the JavaScript code in this page.
*/
const DICO_FR = {
    'enTete1': 'Calculette Eco-déplacements',
    'enTete2': `Calculez l'impact de vos déplacements quotidiens
        sur l\'environnement et vos dépenses`,
    'distance': 'Quelle est la distance entre le domicile et le lieu de travail ?',
    'domicile1': 'J\'habite à ',
    'domicile2': ' km de mon travail.',
    'deplacement': `
        Choisir le mode de déplacement :
        1 - train
        2 - vélo
        3 - voiture
        Mon choix :`
};
const DICO_COUT = {
    'train': [14.62, 9.26],
    'velo': [0, 0],
    'voiture': [129.62, 50.65],
    'labTitre': ["Effet de serre", "Énergie"],
    'labUnite': [" kg eq. CO2", " l eq. pétrole"]
};

/**
* Calcule les coûts de déplacement.
* 
* @param lesCouts liste contenant les éléments kilométriques liés à un mode de déplacement
* @param km distance entre le domicile et le lieu de travail
* @return liste contenant les coûts kilométriques liés à un mode de déplacement
*/
function calculeCoutKm(lesCouts, km) {
    let res = [0, 0];   // Initialiser la liste des résultats
    for (indice in lesCouts) {
        res[indice] = Math.round(lesCouts[indice]*km*100)/100;
    }
    return res;
}
